<?php


namespace App\EventDispatcher;


use App\Event\ProductViewEvent;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

class ProductViewEmailSubscriber implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    protected $logger;
    protected $mailer;


    public function __construct(LoggerInterface $logger, MailerInterface $mailer)
    {
        $this->logger = $logger;
        $this->mailer = $mailer;
    }

    public function sendProductViewEmail(ProductViewEvent $productViewEvent)
    {
//        $email = new TemplatedEmail();
//        $email->from(new Address('contact@mail.com', "Infos de la boutique"))
//            ->to("admin@mail.com")
//            ->htmlTemplate('emails/product_view.html.twig')
//            ->context([
//                'product' => $productViewEvent->getProduct()
//            ])
//            ->subject("Visite du produit numéro " . $productViewEvent->getProduct()->getId());
//
//        $this->mailer->send($email);

        $this->logger->info("Un email a été envoyé pour signaler que le produit numéro "
            . $productViewEvent->getProduct()->getId()
            . " a été vu");
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            'product.view' => 'sendProductViewEmail'
        ];
    }
}