<?php


namespace App\EventDispatcher;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class PrenomSubscriber implements EventSubscriberInterface
{
    public function addPrenomToAttribute(RequestEvent $requestEvent){
        $requestEvent->getRequest()->attributes->set('prenom', 'Bibi');
    }

    public function test1(){
        dump('test1');
    }

    public function test2(){
        dump('test2');
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.request' => 'addPrenomToAttribute',
        ];
    }
}