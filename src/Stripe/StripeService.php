<?php

namespace App\Stripe;

use App\Entity\Purchase;
use Stripe\PaymentIntent;

class StripeService
{
    protected $secretKey;
    protected $publicKey;

    public function __construct(string $secretKey, string $publicKey)
    {
        $this->secretKey = $secretKey;
        $this->publicKey = $publicKey;
    }

    /**
     * @return string
     */
    public function getPublicKey(): string
    {
        return $this->publicKey;
    }

    public function getPaymentIntent(Purchase $purchase) : PaymentIntent{
        // This is your real test secret API key.
        \Stripe\Stripe::setApiKey($this->secretKey);

        $intent = \Stripe\PaymentIntent::create([
            'amount' => $purchase->getTotal(),
            'currency' => 'eur'
        ]);

        return $intent;
    }
}