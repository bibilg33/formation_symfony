<?php


namespace App\Twig;


use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AmountExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('amount', [$this, 'amount'])
        ];
    }

    public function amount($value, string $symbol = '€', string $decimalSeparator = ',', string $thousandSeparator =' ')
    {
//       ex : value = 19229
        $finalValue = $value/100;
        // 192.29
        $finalValue = number_format($finalValue, 2, $decimalSeparator, $thousandSeparator);
        // 192,29
        return $finalValue . $symbol;
        // 192,29 €
    }
}