<?php


namespace App\Taxes;


class Detector
{
    protected $seuil;

    public function __construct($seuil){
        $this->seuil = $seuil;
    }

    public function detect(float $price) : bool{

        if($price > $this->seuil){
            return true;
        }
        return false;
    }

}