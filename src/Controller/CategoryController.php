<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;

class CategoryController extends AbstractController
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function renderMenuList(){

        $categories = $this->categoryRepository->findAll();

        return $this->render('category/_menu.html.twig', [
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/admin/category/create", name="category_create")
     */
    public function create(Request $request, SluggerInterface $slugger, EntityManagerInterface $em): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class,$category);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $category->setSlug(strtolower($slugger->slug($category->getName())));

            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('product_category', [
                'slug' => $category->getSlug(),
            ]);
        }

        return $this->render('category/create.html.twig', [
            'formView' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/category/{id}/edit", name="category_edit")
     */
    public function edit(Category $category, Request $request, EntityManagerInterface $em, SluggerInterface $slugger, Security $security): Response
    {
        if(!$category){
            throw new NotFoundHttpException("Cette catégorie n'existe pas");
        }

//        // Grâce aux voters on peut
//        $this->denyAccessUnlessGranted('CAN_EDIT', $category, "Vous n'êtes pas propriétaire de la catégorie");

//        $user = $security->getUser();
//
//        if($user === null){
//            return $this->redirectToRoute('security_login');
//        }
//
//        if(!in_array("ROLE_ADMIN", $user->getRoles())){
//            throw new AccessDeniedHttpException("Vous n'avez pas le droit d'accéder à cette ressource");
//        }

//        //$security->isGranted
//        if($this->isGranted("ROLE_ADMIN") === false){
//            throw new AccessDeniedHttpException("Vous n'avez pas le droit d'accéder à cette ressource");
//        }

//        $this->denyAccessUnlessGranted("ROLE_ADMIN", null, "Vous n'avez pas le droit d'accéder à cette ressource");

        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $category->setSlug(strtolower($slugger->slug($category->getName())));

            $em->flush();

            return $this->redirectToRoute('product_category', [
                'slug' => $category->getSlug(),
            ]);
        }
        return $this->render('category/edit.html.twig', [
            'category' => $category,
            'formView' => $form->createView(),
        ]);
    }
}
