<?php

namespace App\Controller;

use App\Form\LoginType;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="security_login")
     */
    public function login(AuthenticationUtils $utils): Response
    {
        // Avec le dernier paramètre si ça foire ça remplira le dernier username rentré !! ( marche pas ??)
        $form = $this->createForm(LoginType::class, ['email' => $utils->getLastUsername()]);


        return $this->render('security/login.html.twig', [
            'formView' => $form->createView(),
            // Permet de pouvoir afficher les messages d'erreur
            'error' => $utils->getLastAuthenticationError()
        ]);
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logout(){

    }
}
