<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Event\ProductViewEvent;
use App\Form\ProductType;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\LessThan;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductController extends AbstractController
{
    /**
     * @Route("/{slug}", name="product_category", priority="-1")
     */
    public function category($slug, CategoryRepository $categoryRepository): Response
    {
        $category = $categoryRepository->findOneBy([
            'slug' => $slug
        ]);

        if(!$category){
            throw $this->createNotFoundException("La catégorie demandé n'existe pas");
        }

        return $this->render('product/category.html.twig', [
            'slug' => $slug,
            'category' => $category,
        ]);
    }

    /**
     * @Route("/{category_slug}/{slug}", name="product_show", priority="-1")
     */
    public function show($slug, ProductRepository $productRepository, $prenom, EventDispatcherInterface $dispatcher): Response
    {
//        dd($prenom);
        $product = $productRepository->findOneBy([
            'slug' => $slug
        ]);

        if(!$product){
            throw $this->createNotFoundException("Le produit n'existe pas");
        }

        //Lancer un evnmt qui permet aux autres développeurs de réagir à la vue d'un article
        $productEvent = new ProductViewEvent($product);
        $dispatcher->dispatch($productEvent, 'product.view');


        return $this->render('product/show.html.twig',[
            'product' => $product,
        ]);
    }

    /**
     * @Route("/admin/product/{id}/edit", name="product_edit")
     */
    public function edit($id, ProductRepository $productRepository, Request $request, EntityManagerInterface $em, ValidatorInterface $validator) : Response
    {
//        //Validation d'un objet
//        $product = new Product();
//        $product->setName("Yo mec");
//
//        $resultat = $validator->validate($product);
//
//        if($resultat->count() > 0){
//            dd("Il y a des erreurs", $resultat);
//        }
//        dd('Tout va bien ');


        //Validation tableaux
//        $client = [
//            'nom' => '',
//            'prenom' => 'Lior',
//            'voiture' => [
//                'marque' => '',
//                'couleur' => 'Noire'
//            ]
//        ];
//
//        $collection = new Collection([
//            'nom' => new NotBlank(['message' => "Le nom ne doit pas être vide"]),
//            'prenom' => [
//                new NotBlank(['message' => "Le prénom ne doit pas être vide"]),
//                new Length(['min' => 3, 'minMessage' => "Le prénom doit faire moins de 3 caractères"])
//            ],
//            'voiture' => new Collection([
//                'marque' => new NotBlank(['message' => "La marque de la voiture est obligatoire"]),
//                'couleur' => new NotBlank(['message' => "La couleure de la voiture est obligatoire"])
//            ])
//        ]);
//
//        $resultat = $validator->validate($client, $collection);
//
//        if($resultat->count() > 0){
//            dd("Il y a des erreurs", $resultat);
//        }
//        dd('Tout va bien ');

        // VAlidation données simples :
//        $age = -5;
//
//        $resultat = $validator->validate($age, [
//            new LessThanOrEqual([
//                'value' => 90,
//                'message' => "L'âge doit être inférieur à {{ compared_value }} mais vous avez donné {{ value }}"
//            ]),
//            new GreaterThan([
//                'value' => 0,
//                'message' => "L'âge doit être supérieur à 0"
//            ])
//        ]);
//
//        if($resultat->count() > 0){
//            dd("Il y a des erreurs");
//        }
//        dd('Tout va bien ');

        $product = $productRepository->find($id);

        $form = $this->createForm(ProductType::class, $product);
//        $form->setData($product);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            // PAs de persist car c'est un formulaire d'édition et c'est déjà en base !
            $em->flush();

            return $this->redirectToRoute('product_show', [
                'category_slug' => $product->getCategory()->getSlug(),
                'slug' => $product->getSlug()
            ]);
        }

        return $this->render('product/edit.html.twig', [
           'product' => $product,
            'formView' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/product/create", name="product_create")
     */
    public function create(Request $request, SluggerInterface $slugger, EntityManagerInterface $em) : Response
    {
        $product= new Product();

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $product->setSlug(strtolower($slugger->slug($product->getName())));

            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('product_show', [
                'category_slug' => $product->getCategory()->getSlug(),
                'slug' => $product->getSlug(),
            ]);
        }

        return $this->render('product/create.html.twig',[
            'formView' => $form->createView(),
        ]);
    }
}
