<?php

namespace App\Controller;

use App\Taxes\Calculator;
use App\Taxes\Detector;
use Cocur\Slugify\Slugify;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class HelloController extends AbstractController
{
//    protected $calculator;
//
//    public function __construct(Calculator $calculator){
//        $this->calculator = $calculator;
//    }

    /**
     * @Route("/hello/{name?World}", name="hello")
     */
    public function index($name, LoggerInterface $logger, Calculator $calculator, Slugify $slugify, Detector $detector): Response
    {
//        dump($detector->detect(101));
//        dump($detector->detect(10));
//
//
//        $logger->error("Mon message de log! ");
//
//        $tva = $calculator->calcul(100);
//
//        dump($tva);
//
//        dump($slugify->slugify("Hello World"));

        return $this->render('hello.html.twig',[
            'name' => $name,
            'formateur1' => ['prenom' => 'Lior', 'nom' => 'Chamla'],
            'formateur2' => ['prenom' => 'Jérome', 'nom' => 'Dupont'],
        ]);
    }
}
