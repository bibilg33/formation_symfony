<?php


namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController
{
    /**
     * @Route("/", name="index")
     */
    public function index(){
        dd("ça marche");
    }

    /**
     * @Route("/test/{age<\d+>?0}", name="test", methods={"GET", "POST"},
     *     host="", schemes={"https"})
     */
    public function test(Request $request, $age){

//      On peut l'injecter en dépendance
//        $request = Request::createFromGlobals();

//        dump($request);

//        $age = $request->query->get('age',0);
//
//        On utilise l'objet Request pour analyser la requête et accéder aux superglobales, on fait plus ça :
//        if(!empty($_GET['age'])){
//            $age = $_GET['age'];
//        }

//      On peut le passer directement en paramètre injection dépendance
//        $age = $request->attributes->get('age',0);

//      Un controller doit toujours retourner une Response !
        return new Response("Vous avez $age ans");
    }


}