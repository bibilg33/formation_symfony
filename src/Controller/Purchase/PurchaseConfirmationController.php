<?php

namespace App\Controller\Purchase;

use App\Cart\CartService;
use App\Entity\Purchase;
use App\Entity\PurchaseItem;
use App\Form\CartConfirmationType;
use App\Purchase\PurchasePersister;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

class PurchaseConfirmationController extends AbstractController
{

    protected $cartService;
    protected $manager;

    public function __construct(CartService $cartService, EntityManagerInterface $manager)
    {
        $this->cartService = $cartService;
        $this->manager = $manager;
    }

    /**
     * @Route("/purchase/confirm", name="purchase_confirm")
     * @IsGranted("ROLE_USER", message="Vous devez être connecté pour confirmer une commande")
     */
    public function confirm(Request $request, PurchasePersister $purchasePersister)
    {
        //1 ON veut lire les données du formulaire
        $form = $this->createForm(CartConfirmationType::class);

        $form->handleRequest($request);

        //2 Si le formulaire n'a pas été soumis on dégage
        if(!$form->isSubmitted()){
            //Message flash pour prévenir
            $this->addFlash('warning', 'Vous devez remplir le formulaire de confirmation');
            return $this->redirectToRoute('cart_show');
        }

        //4 Si il n'y a pas de produit dans le panier je dégage aussi (SessionInterface ou CartService)
        $cartItems = $this->cartService->getDetailedCartItems();

        if(count($cartItems) === 0){
            $this->addFlash('warning', 'Vous ne pouvez pas confirler avec un panier vide');

            return $this->redirectToRoute('cart_show');
        }

        //5 On crée une purchase
        //On peut faire ça car on a précisé data_class dans formulaire
        /** @var $purchase Purchase */
        $purchase = $form->getData();

        $purchasePersister->storePurchase($purchase);

        return $this->redirectToRoute('purchase_payment_form', [
            'id' => $purchase->getId()
        ]);

    }
}