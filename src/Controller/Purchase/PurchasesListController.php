<?php

namespace App\Controller\Purchase;

use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;

class PurchasesListController extends AbstractController{


    /**
     * @Route("/purchases", name="purchase_index")
     * @IsGranted("ROLE_USER", message="Vous devez être connecté pour accéder à vos commandes")
     */
    public function index(){
        //1 On s'&ssure que la personne est co sinon redirection vers la page d'accueil
        /* @var $user User */
        $user = $this->getUser();

        //On remplace ça par IsGranted
//        if(!$user){
////            $url = $this->router->generate('homepage');
////            return new RedirectResponse($url);
//            throw new AccessDeniedException('Vous devez être connecté pour accéder à vos commandes');
//        }

        //2 Qui est connecte (On  l'a grâce à getUser)
        //3 ON passe l'utilisateur connecté à twig pour afficher ses commandes
        return $this->render('purchase/index.html.twig', [
            'purchases' => $user->getPurchases(),
        ]);
    }

}