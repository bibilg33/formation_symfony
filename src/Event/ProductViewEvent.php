<?php


namespace App\Event;


use App\Entity\Product;
use Symfony\Contracts\EventDispatcher\Event;

class ProductViewEvent extends Event
{
    private $product;

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

}