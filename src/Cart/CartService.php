<?php

namespace App\Cart;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CartService{

    protected $session;
    protected $productRepository;

    public function __construct(SessionInterface $session, ProductRepository $productRepository)
    {
        $this->session = $session;
        $this->productRepository = $productRepository;
    }

    protected function getCart() : array
    {
        return $this->session->get('cart', []);
    }

    protected function setCart(array $cart)
    {
        $this->session->set('cart', $cart);
    }

    public function empty(){
        $this->setCart([]);
    }

    public function decrement(int $id){
        //On récupère le tableau qui gère le panier
        $cart = $this->getCart();

        if(!array_key_exists($id, $cart)){
            //Si ça existe pas on fait rien
            return;
        }

        if($cart[$id] === 1){
            $this->remove($id);
            return;
        }

        //Dernière option:
        $cart[$id]--;

        $this->setCart($cart);
    }

    public function remove(int $id){
        $cart = $this->getCart();

        unset($cart[$id]);

        $this->setCart($cart);
    }

    public function add(int $id){

        // Si le tableu n'existe pas on crée [] par défaut
//        $cart = $this->session->get('cart', []);
        $cart = $this->getCart();

        // Si le produit est pas dans le tableau, on le crée
        if(!array_key_exists($id, $cart)){
            $cart[$id]=0;
        }

        $cart[$id] += 1;


        //Ensuite on envoie le tableau cart mis à jour à la session
        $this->setCart($cart);

    }

    public function getTotal() : int{

        $total = 0;

        foreach ($this->getCart() as $id => $qty){
            $product = $this->productRepository->find($id);

            if(!$product){
                continue;
            }

            $total += $product->getPrice() * $qty;
        }

        return $total;

    }

    /**
     * @return array<CartItem>
     */
    public function getDetailedCartItems() : array{
        $detailedCart = [];

        // On veut : [ 'id_produit' => ['product' => ....., 'quantity' => quantité] ]

        foreach ($this->getCart() as $id => $qty){
            $product = $this->productRepository->find($id);

            if(!$product){
                continue;
            }

            $detailedCart[] = new CartItem($product, $qty);
        }

        return $detailedCart;
    }
}